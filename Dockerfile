FROM python:2.7
ADD test.py /
RUN pip install pandas
CMD ["python","./test.py"]