#!/bin/bash

VERSION=1.0.0

if [ "$1" != "" ]; then
  VERSION=$1
fi

docker build --build-arg version=$VERSION --force-rm -t docker.artifactory.infra.base.cxl.io/test-processor:$VERSION .
docker tag docker.artifactory.infra.base.cxl.io/cxl/test-processor:$VERSION docker.artifactory.infra.base.cxl.io/test-processor:latest

docker login docker.artifactory.infra.base.cxl.io
docker push docker.artifactory.infra.base.cxl.io/test-processor:latest